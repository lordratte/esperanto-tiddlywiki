![Build Status](https://gitlab.com/lordratte/esperanto-tiddlywiki/badges/master/pipeline.svg)

A TiddlyWiki project containing a work-in-progress translation to Esperanto.


To make changes, either start the server:
```
npm install -g tiddlywiki
tiddlywiki src --listen
```

Or edit the tiddlers in `src` directly.
